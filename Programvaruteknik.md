# Programvaruteknik vid Mittuniversitetet

Hösten 2021 tar vi åter in nya studenter till vårt populära program
Programvaruteknik vid Mittuniversitetet i Östersund. Nytt är att du kan 
läsa programmet på Campus i Östersund eller på distans! Fokus i programmet 
är att kunna bygga olika typer av applikationer för Webben, din dator eller 
för din smartphone. Mobilitet, distansoberoende, webbtjänster och cloud 
computing ökar i betydelse och där är vi med! Genom att läsa programmet 
skaffar du dig en gedigen universitetsutbildning som ger dig verktygen att 
vara med att forma den digitala framtiden.

Utbildningen erbjuder dig en språngbräda till ett arbete inom IT-branschen. 
Med modern teknik och metodik lär du dig design och implementering av 
mjukvara för system som kan bestå av såväl servrar, persondatorer/laptops 
som mobiler och plattor. Programmet har utvecklats i samarbete med 
IT-företag. För att läsa programmet på distans behöver du en bra 
internetuppkoppling, ett headset och en webbkamera för att kunna 
kommunicera. Mötena är alltså virtuella och du behöver inte förflytta dig 
till oss i Östersund.

Läser du på campus har du naturligtvis tillgång till allt distansmaterial 
utöver alla aktiviteterna på plats i vår egen labbsal. Nytt för i år är 
helt nya lokaler på första våningen med eget klassrum/labbsal. Vi delar 
pentry, soffa och studiehörna med Informatik-gänget. De är ju helt 
underlägsna oss i programmering men det får väl gå ändå:)

Det första året innehåller nödvändiga grundkurser som ska leda fram till 
en gedigen kompetens inom området objektorienterad programmering. Det 
andra året ägnas åt olika aspekter av utveckling för webben med kopplingar 
till databaser, överbryggning från C++ till Java samt tillämpningar inom 
området mobila enheter. Under utbildningen får du även arbeta i grupper med 
ett gemensamt ansvar för olika delar i kedjan från idé till färdig produkt.

Efter tre år kan du ta ut en kandidatexamen med datateknik som huvudämne 
och med inriktning mot programvaruteknik. Du kan alternativt ta ut en 
högskoleexamen med inriktning mot datateknik efter två år om du väljer att 
inte läsa det tredje året.

Under utbildningen kommer du mest att läsa kurser inom ämnesområdet 
datateknik. Ditt virtuella klassrum i de olika kurserna kommer att vara 
den webbaserade utbildningsplattformen Moodle. Där hittar du allt 
kursmaterial och där kommunicerar du med lärare och dina medstudenter på 
kurserna. För det mesta görs också prov och redovisningar via webben.

Våra kurser innehåller, förutom teori, en stor mängd praktiska uppgifter 
som ska utföras och redovisas. Ofta handlar det om programmeringsuppgifter 
som du ska lösa själv eller mindre projekt som du kan samarbeta med andra 
studenter om. Många av kurserna bygger vidare på tidigare kurser och på så 
vis fördjupas kunskaperna samtidigt som du tillämpar kunskaper från tidigare 
kurser. Eftersom allt kursmaterial kommer att finnas tillgängligt i Moodle 
kommer du inte att vara bunden att passa tider för föreläsningar mm. Däremot 
kommer redovisningar och prov att vara bundna till vissa tider.

## År 1

- Datavetenskaplig introkurs (LP1)
- Operativsystem introduktion med tillämpningar i Linux (LP1)
- Introduktion till programmering i Java (LP2)
- Datakommunikation och nätverk med tillämpningar i Linux (LP2)
- Objektbaserad programmering i Java (LP3)
- Diskret matematik för programmerare (LP3+4)
- Databaser, modellering och implementering (LP3+4)
- Objektorienterad programmering i Java (LP4)
- Valbar kurs
    


## År 2

- Metoder och verktyg i mjukvaruprojekt (LP1)
- XML (LP1)
- Java för C++ programmerare (LP2)
- Webbprogrammering med HTML5, CSS3 och JavaScript (LP2)
- Designmönster med Java (LP3)
- Webbprogrammering med PHP och PostgreSQL (LP3)
- Applikationsutveckling för Android (LP4)
- Säkerhet i mjukvara (LP4)
- Valbar kurs


## År 3 

- Valbar kurs
- Presentation av ny teknik (LP1+2)
- Java Enterprise-utveckling med EE-standarden (LP1+2)
- Systemprogrammering i UNIX/Linux (LP1)
- Programmering med samtidighet och parallellism (LP2)
- Artificiell Intelligens för agenter (LP3)
- Tillämpad datateknik, mjukvaruprojekt (LP3)
- Sjävständigt arbete (LP4)

