'''
File containing some functions
'''

def bit_to_int(bits):
    i = len(bits) - 1
    answer = 0
    for bit in bits:
        if bit == "1":
            answer += 2 ** i
        i -= 1
    print(answer)


def bit_to_hex(bits):
    bits = [bits[i:i+4] for i in range(0, len(bits), 4)]   # divide input into 4 bit strings
    hex = "0123456789ABCDEF"
    result = []
    for fourbits in bits:
        num = 3
        answer = 0
        if len(fourbits) != 4: break   # only complete 4-bit binary numbers will get result
        for bit in fourbits:
            if bit == "1":
                answer += 2 ** num
            num -= 1
        result.append(hex[answer])
    print(result)